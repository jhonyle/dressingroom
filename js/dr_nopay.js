var TEMPLATE = function () {},
    RECTANGLE = function() {};

TEMPLATE.prototype = {
    id: 0,
    img_of_template_src: '',
    rectangles: new Array()
};

RECTANGLE.prototype = {
    name: '',
    width: 0,
    height: 0,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    standart_img_src: '',
    this_block: '',
    this_img: ''
};

// interface for templates
function Interface() {
    var block;
}

//    first_step
Interface.prototype.first_step = function()
{
    var tmp = this;
    $("body").append(
        $("<div>")
            .addClass("what_kind_of_creation")
            .append(
                $("<div>")
                    .addClass("kind_of_creation first")
                    .html("Создать из шаблона")
                    .click(
                        function ()
                        {
                            $(".what_kind_of_creation").remove();

                            // creation templates
                            var temps;
                            $("body").append(temps =
                                $("<div>")
                                    .addClass("what_kind_of_template")
                            );
                            if (templates && templates.length != 0)
                            {
                                for (var i = 0; i < templates.length; i++)
                                {
                                    temps.append(
                                        $("<img>")
                                            .attr("src", templates[i].prototype.img_of_template_src)
                                            .attr("title", templates[i].prototype.id)
                                            .addClass("img_template")
                                            .click(function()
                                            {
                                                idt = $(this).attr("title");
                                                tmp.open_template(idt);
                                            })
                                    )
                                }
                            }
                            else
                            {
                                alert("Ошибка! Шаблоны не найдены!")
                            }
                        }
                    )
            )
    )
};

//    open template
Interface.prototype.open_template = function(id)
{
    var tmp = this;
    $("#dialog").dialog({ autoOpen: false });


    $("#dialog .button.add").click(function() {
        block.find("img").attr("src", $("#dialog input").val());
        $("#dialog").dialog("close");
        tmp.updateimgs(block.find("img"));
    });

    $("#save").click(function() {
        var list = '';
        $(".wrapper_dr .new-block img").each(function() {
            list += $(this).css("width") + "|" + $(this).css("height") + "|" + $(this).css("top") + "|" + $(this).css("left") + "|" + $(this).css("right") + "|" + $(this).css("bottom") + "|" + $(this).attr("src");
        });
    });
    var rectangles = templates[id].prototype.rectangles, rec;
    for (var i = 0; i < rectangles.length; i++)
    {
        $(".this_is_conteiner").append(rec =
            $("<div>")
                .addClass("new-block " + rectangles[i].name)
                .css(
                    {
                        width: rectangles[i].width + "px",
                        height: rectangles[i].height + "px",
                        top: rectangles[i].top + "px",
                        left: rectangles[i].left + "px",
                        bottom: rectangles[i].bottom + "px",
                        right: rectangles[i].right + "px"
                    }
                )
                .dblclick(function()
                {
                    block = $(this);
                    $("#dialog input").val($(this).find("img").attr("src"));
                    $("#dialog").attr("block", $(this));
                    $("#dialog").dialog("open");
                })
            );
            rec.append(
                $("<img>")
                    .attr("src", rectangles[i].standart_img_src)
            );
        $(".what_kind_of_template").remove();
    }
    this.updateimgs();
};


// resize images
Interface.prototype.updateimgs = function(img)
{
    if (typeof img != 'undefined')
    {
        if (img.width() > img.height())
        {
            img.css({width: "100%"});
        }
        else
        {
            img.css({height: "100%"});
        }
        img.css({marginLeft: - img.width() / 2, marginTop: - img.height() / 2});
    }
    else
    {
        $(".this_is_conteiner .new-block img").each(function() {
            if ($(this).width() > $(this).height())
            {
                $(this).css({width: "100%"});
            }
            else
            {
                $(this).css({height: "100%"});
            }
            $(this).css({marginLeft: - $(this).width() / 2, marginTop: - $(this).height() / 2});
        });
    }
};


// decriptions of templates
var template1 = new TEMPLATE(), template2 = new TEMPLATE(), template3 = new TEMPLATE(), template4 = new TEMPLATE(), templates = [template1, template2, template3, template4];

template1.prototype = {
    id: 0,
    img_of_template_src: "img/1.png",
    rectangles:
        [
            {
                name: 'shurt',
                width: 300,
                height: 220,
                standart_img_src: 'img/shurt.png'
            },
            {
                name: 'belt',
                width: 200,
                height: 100,
                top: 50,
                right: 50,
                standart_img_src: 'img/belt.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 220,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'djins',
                width: 300,
                height: 380,
                top: 220,
                left: 0,
                standart_img_src: 'img/djins.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 440,
                right: 80,
                standart_img_src: 'img/shoes.png'
            }
        ]

};

template2.prototype = {
    id: 1,
    img_of_template_src: "img/2.png",
    rectangles:
        [
            {
                name: 'dress',
                width: 300,
                height: 600,
                standart_img_src: 'img/dress.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 220,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 440,
                right: 80,
                standart_img_src: 'img/shoes.png'
            }
        ]
};

template3.prototype = {
    id: 2,
    img_of_template_src: "img/3.png",
    rectangles:
        [
            {
                name: 'dress',
                width: 300,
                height: 600,
                standart_img_src: 'img/dress.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 260,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 470,
                right: 80,
                standart_img_src: 'img/shoes.png'
            },
            {
                name: 'bijouterie nailpolish',
                width: 120,
                height: 100,
                top: 135,
                right: 150,
                standart_img_src: 'img/nailpolish.png'
            },
            {
                name: 'bijouterie shadow',
                width: 120,
                height: 100,
                top: 135,
                right: 20,
                standart_img_src: 'img/shadow.png'
            },
            {
                name: 'bijouterie pendant',
                width: 120,
                height: 100,
                top: 30,
                right: 155,
                standart_img_src: 'img/pendant.png'
            },
            {
                name: 'bijouterie bracelet',
                width: 120,
                height: 100,
                top: 30,
                right: 20,
                standart_img_src: 'img/bracelet.png'
            }
        ]
};

template4.prototype = {
    id: 3,
    img_of_template_src: "img/4.png",
    rectangles:
        [
            {
                name: 'shurt',
                width: 300,
                height: 220,
                standart_img_src: 'img/shurt.png'
            },
            {
                name: 'belt',
                width: 180,
                height: 100,
                top: 490,
                right: 120,
                standart_img_src: 'img/belt.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 260,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'djins',
                width: 300,
                height: 380,
                top: 220,
                left: 0,
                standart_img_src: 'img/djins.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 480,
                right: 0,
                standart_img_src: 'img/shoes.png'
            },
            {
                name: 'bijouterie nailpolish',
                width: 120,
                height: 100,
                top: 135,
                right: 150,
                standart_img_src: 'img/nailpolish.png'
            },
            {
                name: 'bijouterie shadow',
                width: 120,
                height: 100,
                top: 135,
                right: 20,
                standart_img_src: 'img/shadow.png'
            },
            {
                name: 'bijouterie pendant',
                width: 120,
                height: 100,
                top: 30,
                right: 155,
                standart_img_src: 'img/pendant.png'
            },
            {
                name: 'bijouterie bracelet',
                width: 120,
                height: 100,
                top: 30,
                right: 20,
                standart_img_src: 'img/bracelet.png'
            }
        ]
};


$(window).load(function() {

    var inter = new Interface();
    inter.first_step();

});
