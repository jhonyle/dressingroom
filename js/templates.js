var TEMPLATE = function () {},
    RECTANGLE = function() {};

TEMPLATE.prototype = {
    id: 0,
    img_of_template_src: '',
    rectangles: new Array()
};

RECTANGLE.prototype = {
    name: '',
    width: 0,
    height: 0,
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    standart_img_src: '',
    this_block: '',
    this_img: ''
};


var template1 = new TEMPLATE(), template2 = new TEMPLATE(), template3 = new TEMPLATE(), template4 = new TEMPLATE(), templates = [template1, template2, template3, template4];

// decriptions of templates

template1.prototype = {
    id: 0,
    img_of_template_src: "img/1.png",
    rectangles:
        [
            {
                name: 'shurt',
                width: 300,
                height: 220,
                standart_img_src: 'img/shurt.png'
            },
            {
                name: 'belt',
                width: 200,
                height: 100,
                top: 50,
                right: 50,
                standart_img_src: 'img/belt.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 220,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'djins',
                width: 300,
                height: 380,
                top: 220,
                left: 0,
                standart_img_src: 'img/djins.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 440,
                right: 80,
                standart_img_src: 'img/shoes.png'
            }
        ]

};

template2.prototype = {
    id: 1,
    img_of_template_src: "img/2.png",
    rectangles:
        [
            {
                name: 'dress',
                width: 300,
                height: 600,
                standart_img_src: 'img/dress.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 220,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 440,
                right: 80,
                standart_img_src: 'img/shoes.png'
            }
        ]
};

template3.prototype = {
    id: 2,
    img_of_template_src: "img/3.png",
    rectangles:
        [
            {
                name: 'dress',
                width: 300,
                height: 600,
                standart_img_src: 'img/dress.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 260,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 470,
                right: 80,
                standart_img_src: 'img/shoes.png'
            },
            {
                name: 'bijouterie nailpolish',
                width: 120,
                height: 100,
                top: 135,
                right: 150,
                standart_img_src: 'img/nailpolish.png'
            },
            {
                name: 'bijouterie shadow',
                width: 120,
                height: 100,
                top: 135,
                right: 20,
                standart_img_src: 'img/shadow.png'
            },
            {
                name: 'bijouterie pendant',
                width: 120,
                height: 100,
                top: 30,
                right: 155,
                standart_img_src: 'img/pendant.png'
            },
            {
                name: 'bijouterie bracelet',
                width: 120,
                height: 100,
                top: 30,
                right: 20,
                standart_img_src: 'img/bracelet.png'
            }
        ]
};

template4.prototype = {
    id: 3,
    img_of_template_src: "img/4.png",
    rectangles:
        [
            {
                name: 'shurt',
                width: 300,
                height: 220,
                standart_img_src: 'img/shurt.png'
            },
            {
                name: 'belt',
                width: 180,
                height: 100,
                top: 490,
                right: 120,
                standart_img_src: 'img/belt.png'
            },
            {
                name: 'bag',
                width: 200,
                height: 200,
                top: 260,
                right: 50,
                standart_img_src: 'img/bag.png'
            },
            {
                name: 'djins',
                width: 300,
                height: 380,
                top: 220,
                left: 0,
                standart_img_src: 'img/djins.png'
            },
            {
                name: 'shoes',
                width: 120,
                height: 120,
                top: 480,
                right: 0,
                standart_img_src: 'img/shoes.png'
            },
            {
                name: 'bijouterie nailpolish',
                width: 120,
                height: 100,
                top: 135,
                right: 150,
                standart_img_src: 'img/nailpolish.png'
            },
            {
                name: 'bijouterie shadow',
                width: 120,
                height: 100,
                top: 135,
                right: 20,
                standart_img_src: 'img/shadow.png'
            },
            {
                name: 'bijouterie pendant',
                width: 120,
                height: 100,
                top: 30,
                right: 155,
                standart_img_src: 'img/pendant.png'
            },
            {
                name: 'bijouterie bracelet',
                width: 120,
                height: 100,
                top: 30,
                right: 20,
                standart_img_src: 'img/bracelet.png'
            }
        ]
};
