var block, img;

function updateimgs(img)
{
    if (typeof img != 'undefined')
    {
        if (img.width() > img.height())
        {
            img.css({width: "100%"});
        }
        else
        {
            img.css({height: "100%"});
        }
//        img.css({top: "50%", left: "50%", marginLeft: - img.width() / 2, marginTop: - img.height() / 2});
    }
}

$(window).load(function() {
    $("#dialog").dialog({ autoOpen: false });

    $("#dialog .button.add").click(function() {
        img = block.find("img");
        img.attr("src", $("#dialog input").val());
        block.height(img.height());
        block.width(img.width());
        updateimgs(img);
        $("#dialog").dialog("close");
    });

    $("#dialog .button.del").click(function() {
        block.remove();
        $("#dialog").dialog("close");
    });

    $("#add-new-block").click(function() {
        $(".wrapper_dr")
            .append(
                $("<div></div>")
                    .addClass("new-block")
                    .draggable({ stack: ".new-block", containment: ".wrapper_dr", scroll:false })
                    .resizable({ helper:"ui-resizable-helper", aspectRatio:true })
                    .append(
                        img = $("<img>")
                            .attr("src", "img/img.png")
                    )
                    .dblclick(function()
                    {
                        block = $(this);
                        $("#dialog input").val($(this).find("img").attr("src"));
                        $("#dialog").attr("block", $(this));
                        $("#dialog").dialog("open");
                    })
            );
        updateimgs(img);


    });

});
